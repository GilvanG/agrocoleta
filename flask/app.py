from flask import Flask
from waitress import serve
from flask import render_template
from flask import request, url_for, redirect, flash, make_response
from flask_bootstrap import Bootstrap
from flask_wtf.csrf import CSRFProtect
import logging
import os
import datetime
from formUsuario import ColetorForm
from formProduto import ProdutoForm
from formCampo import CampoForm
from formColeta import ColetaForm
from flask_session import Session
from flask import session
from formLogin import LoginForm
import hashlib
import json
from flask_json import FlaskJSON, JsonError, json_response, as_json

app = Flask(__name__)
bootstrap = Bootstrap(app)
CSRFProtect(app)
CSV_DIR = '/flask/'

app.config['SESSION_TYPE'] = 'filesystem'
app.config['SECRET_KEY'] = os.urandom(24)
app.config['WTF_CSRF_SSL_STRICT'] = False
Session(app)
FlaskJSON(app)
app.config['JSON_ADD_STATUS'] = False

logging.basicConfig(filename=CSV_DIR + 'app.log', filemode='w',
                    format='%(asctime)s %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + CSV_DIR + 'bd.sqlite3'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['TEMPLATES_AUTO_RELOAD'] = True

from database import db

db.init_app(app)

from Coletor import Coletor
from Campo import Campo
from Produto import Produto
from Coleta import Coleta


@app.before_first_request
def inicializar_bd():
    # db.drop_all()

    db.create_all()


@app.route('/')
def root():
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    return (render_template('index.html'))


# USUARIO

@app.route('/usuario/logout', methods=['POST', 'GET'])
def logout():
    session.clear()
    return (redirect(url_for('login')))


@app.route('/usuario/login', methods=['POST', 'GET'])
def login():
    form = LoginForm()
    if not session.get('autenticado', False) == False:
        return (redirect(url_for('root')))
    if form.validate_on_submit():
        # PROCESSAMENTO DOS DADOS RECEBIDOS
        cpf = request.form['cpf']
        senha = request.form['senha']
        senhahash = hashlib.sha1(senha.encode('utf8')).hexdigest()
        # Verificar se existe alguma linha na tabela usuários com o login e senha recebidos
        linha = Coletor.query.filter(Coletor.cpf == cpf, Coletor.senha == senhahash).all()
        if (len(linha) > 0):  # "Anota" na sessão que o usuário está autenticado
            session['autenticado'] = True
            session['usuario'] = linha[0].id
            flash(u'Usuário autenticado com sucesso!')
            resp = make_response(redirect(url_for('root')))
            if 'contador' in request.cookies:
                contador = int(request.cookies['contador'])
                contador = contador + 1
            else:
                contador = 1
            resp.set_cookie('contador', str(contador))
            return (resp)
        else:  # Usuário e senha não conferem
            flash(u'Usuário e/ou senha não conferem!')
            resposta = make_response(redirect(url_for('login')))
            if 'contador2' in request.cookies:
                contador2 = int(request.cookies['contador2'])
                contador2 = contador2 + 1
            else:
                contador2 = 1
            resposta.set_cookie('contador2', str(contador2))
            return (resposta)
    return (render_template('formLogin.html', form=form, action=url_for('login')))


@app.route('/usuario/cadastrar', methods=['POST', 'GET'])
def cadastrar_usuario():
    # if session.get('autenticado',False)==False:
    #     return (redirect(url_for('login')))
    form = ColetorForm()
    if form.validate_on_submit():
        # PROCESSAMENTO DOS DADOS RECEBIDOS

        cpf = request.form['cpf']
        nome = request.form['nome']
        sobrenome = request.form['sobrenome']
        email = request.form['email']
        senha = request.form['senha']
        senhahash = hashlib.sha1(senha.encode('utf8')).hexdigest()

        novoUsuario = Coletor(cpf=cpf, nome=nome, sobrenome=sobrenome, email=email, senha=senhahash)
        db.session.add(novoUsuario)
        db.session.commit()
        flash(u'Usuário cadastrado com sucesso!')
        return (redirect(url_for('root')))
    return (render_template('formRegister.html', form=form, action=url_for('cadastrar_usuario')))


@app.route('/usuario/listar')
def listar_coletores():
    coletores = Coletor.query.order_by(Coletor.id).all()
    return (render_template('coletores.html', coletores=coletores))


@app.route('/coletor/remover/<id_coletor>', methods=['GET', 'POST'])
def remover_coletor(id_coletor):
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    id_coletor = int(id_coletor)
    coletor = Coletor.query.get(id_coletor)
    db.session.delete(coletor)
    db.session.commit()
    return (redirect(url_for('logout')))


# CAMPO

@app.route('/campo/cadastrar', methods=['POST', 'GET'])
def cadastrar_campo():
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    form = CampoForm()
    if form.validate_on_submit():
        # PROCESSAMENTO DOS DADOS RECEBIDOS

        nome = request.form['nome']
        descricao = request.form['descricao']

        novoCampo = Campo(nome=nome, descricao=descricao)
        db.session.add(novoCampo)
        db.session.commit()
        flash(u'Campo cadastrado com sucesso!')
        return (redirect(url_for('root')))
    return (render_template('form.html', form=form, action=url_for('cadastrar_campo')))


@app.route('/campo/listar')
def listar_campos():
    campos = Campo.query.order_by(Campo.id).all()
    return (render_template('campos.html', campos=campos))


@app.route('/campo/remover/<id_campo>', methods=['GET', 'POST'])
def remover_campo(id_campo):
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    id_campo = int(id_campo)
    campo = Campo.query.get(id_campo)
    db.session.delete(campo)
    db.session.commit()
    return (redirect(url_for('root')))


# PRODUTO

@app.route('/produto/cadastrar', methods=['POST', 'GET'])
def cadastrar_produto():
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    form = ProdutoForm()
    if form.validate_on_submit():
        # PROCESSAMENTO DOS DADOS RECEBIDOS
        nome = request.form['nome']
        peso = float(request.form['peso'])
        especie = request.form['especie']

        novoProduto = Produto(nome=nome, peso=peso, especie=especie)
        db.session.add(novoProduto)
        db.session.commit()
        flash(u'Produto cadastrado com sucesso!')
        return (redirect(url_for('root')))
    return (render_template('form.html', form=form, action=url_for('cadastrar_produto')))


@app.route('/produto/listar')
def listar_produtos():
    produtos = Produto.query.order_by(Produto.id).all()
    return (render_template('produtos.html', produtos=produtos))


@app.route('/produto/remover/<id_produto>', methods=['GET', 'POST'])
def remover_produto(id_produto):
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    id_produto = int(id_produto)
    produto = Produto.query.get(id_produto)
    db.session.delete(produto)
    db.session.commit()
    return (redirect(url_for('root')))


# COLETA

@app.route('/coleta/cadastrar', methods=['POST', 'GET'])
def cadastrar_coleta():
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    form = ColetaForm()

    coletores = Coletor.query.order_by(Coletor.nome).all()
    campos = Campo.query.order_by(Campo.nome).all()
    produtos = Produto.query.order_by(Produto.nome).all()

    form.campo.choices = [(c.id, c.nome) for c in campos]
    form.coletor.choices = [(c.id, c.nome) for c in coletores]
    form.produto.choices = [(p.id, p.nome) for p in produtos]

    if form.validate_on_submit():
        # PROCESSAMENTO DOS DADOS RECEBIDOS
        coletor = int(request.form['coletor'])
        campo = int(request.form['campo'])
        produto = int(request.form['produto'])
        quantidade = request.form['quantidade']
        peso = float(request.form['peso'])
        observacoes = request.form['observacoes']

        novaColeta = Coleta(id_coletor=coletor, id_campo=campo, id_produto=produto, quantidade=quantidade, peso=peso,
                            observacoes=observacoes)
        db.session.add(novaColeta)
        db.session.commit()
        flash(u'Coleta cadastrado com sucesso!')
        return (redirect(url_for('root')))
    return (render_template('form.html', form=form, action=url_for('cadastrar_coleta')))


@app.route('/coleta/listar')
def listar_coletas():
    coletas = Coleta.query.order_by(Coleta.id).all()
    return (render_template('coletas.html', coletas=coletas))


@app.route('/coleta/remover/<id_coleta>', methods=['GET', 'POST'])
def remover_coleta(id_coleta):
    if session.get('autenticado', False) == False:
        return (redirect(url_for('login')))
    id_coleta = int(id_coleta)
    coleta = Coleta.query.get(id_coleta)
    db.session.delete(coleta)
    db.session.commit()
    return (redirect(url_for('root')))


# JSON

@app.route('/json/coletor')
def listar_coletores_json():
    coletores = Coletor.query.order_by(Coletor.id).all()
    resultado = json.dumps([row.asdict() for row in coletores])
    return (resultado)


@app.route('/json/campo')
def listar_campos_json():
    campos = Campo.query.order_by(Campo.id).all()
    resultado = json.dumps([row.asdict() for row in campos])
    return (resultado)


@app.route('/json/produto')
def listar_produtos_json():
    produtos = Produto.query.order_by(Produto.id).all()
    resultado = json.dumps([row.asdict() for row in produtos])
    return (resultado)


@app.route('/json/coleta')
def listar_coletas_json():
    coletas = Coleta.query.order_by(Coleta.id).all()
    resultado = json.dumps([row.asdict() for row in coletas], indent=4, sort_keys=True, default=str)
    return (resultado)


@app.errorhandler(404)
def pageNotFound(error):
    return "PÁGINA NÃO ENCONTRADA"


if __name__ == "__main__":
    serve(app, host='0.0.0.0', port=80, url_prefix='/app')

from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class CampoForm(FlaskForm):
    nome = StringField('Nome do Campo', validators=[DataRequired()])
    descricao = StringField('Descrição do Campo', validators=[DataRequired()])
    enviar = SubmitField('CADASTRAR')

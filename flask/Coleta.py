from sqlalchemy.orm import backref
from database import db
from sqlalchemy.sql import func


class Coleta(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_coletor = db.Column(db.Integer, db.ForeignKey('coletor.id'))
    id_campo = db.Column(db.Integer, db.ForeignKey('campo.id'))
    id_produto = db.Column(db.Integer, db.ForeignKey('produto.id'))
    quantidade = db.Column(db.Integer, unique=False, nullable=False)
    peso = db.Column(db.Float, unique=False, nullable=False)
    data_coleta = db.Column(db.DateTime, unique=False, nullable=False, default=func.now())
    observacoes = db.Column(db.String(200), nullable=True)

    def asdict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

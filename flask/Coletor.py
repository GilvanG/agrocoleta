from database import db

class Coletor(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    cpf = db.Column(db.String(15), unique=False, nullable=False)
    nome = db.Column(db.String(100), unique=False, nullable=False)
    sobrenome = db.Column(db.String(200), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=True)
    senha = db.Column(db.String(80), unique=False, nullable=False)
    coleta_produto = db.relationship('Coleta',backref='coletor',lazy=True)

    def asdict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

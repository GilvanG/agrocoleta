from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired


class ProdutoForm(FlaskForm):
    nome = StringField('Nome do Produto', validators=[DataRequired()])
    peso = StringField('Peso Padrão do(s) Produto(s)', validators=[DataRequired()])
    especie = StringField('Especie do Produto', validators=[DataRequired()])
    enviar = SubmitField('CADASTRAR')

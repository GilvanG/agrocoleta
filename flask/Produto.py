from database import db


class Produto(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(20), unique=False, nullable=False)
    peso = db.Column(db.Float, unique=False, nullable=False)
    especie = db.Column(db.String(200), nullable=True)
    coleta_produto = db.relationship('Coleta', backref='produto', lazy=True)

    def asdict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

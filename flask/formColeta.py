from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, SelectField
from wtforms.validators import DataRequired


class ColetaForm(FlaskForm):
    coletor = SelectField('Coletor', coerce=int)
    campo = SelectField('Campo', coerce=int)
    produto = SelectField('Produto', coerce=int)
    quantidade = StringField('Quantidade Coletada: ', validators=[DataRequired()])
    peso = StringField('Peso Medio da Coleta: ', validators=[DataRequired()])
    observacoes = StringField('Observações: ', validators=[DataRequired()])
    enviar = SubmitField('CADASTRAR')

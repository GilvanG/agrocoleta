from app import app
import pytest

aplicacao = app.test_client()


def login(client, username):
    return client.post('/usuario/login', data=dict(
        cpf=username,
        senha=username
    ), follow_redirects=True)


def logout(client):
    return client.get('/usuario/logout', follow_redirects=True)


@pytest.fixture
def client():
    return (aplicacao)


@pytest.fixture
def preparacao():
    app.config['WTF_CSRF_ENABLED'] = False
    yield
    app.config['WTF_CSRF_ENABLED'] = True


def test_0_principal(client):
    rv = client.get('/', follow_redirects=True)
    assert 200 == rv.status_code


def test_1_principal(client):
    rv = client.get('/', follow_redirects=True)
    assert b'CPF' in rv.data
    assert b'Senha:' in rv.data


@pytest.mark.usefixtures('preparacao')
def test_2_login(client):
    rv = login(client, '123.123.123-13')
    assert b'autenticado com sucesso' in rv.data


def test_3_campo_cadastrar_get(client):
    rv = client.get('/campo/cadastrar', follow_redirects=True)
    assert b'Nome do Campo' in rv.data
    assert b'Descri' in rv.data


def test_4_campo_listar(client):
    rv = client.get('/campo/listar', follow_redirects=True)
    assert rv.status_code == 200


@pytest.mark.usefixtures('preparacao')
def test_5_login_errado(client):
    rv = login(client, 'UsuarioNaoExistente')
    assert b'rio e/ou senha' in rv.data


def test_6_usuario_listar(client):
    rv = client.get('/usuario/listar', follow_redirects=True)
    assert rv.status_code == 200


def test_7_produto_cadastrar_get(client):
    rv = client.get('/produto/cadastrar', follow_redirects=True)
    assert b'Nome do Produto' in rv.data
    assert b'Peso Padr' in rv.data
    assert b'Especie do Produto' in rv.data


def test_8_produto_listar(client):
    rv = client.get('/produto/listar', follow_redirects=True)
    assert rv.status_code == 200


def test_9_coleta_cadastrar_get(client):
    rv = client.get('/coleta/cadastrar', follow_redirects=True)
    assert b'Coletor' in rv.data
    assert b'Campo' in rv.data
    assert b'Produto' in rv.data
    assert b'Quantidade Coletada:' in rv.data
    assert b'Peso Medio da Coleta:' in rv.data
    assert b'Observa' in rv.data


def test_10_coleta_listar(client):
    rv = client.get('/coleta/listar', follow_redirects=True)
    assert rv.status_code == 200


def test_11_coletor_cadastrar_get(client):
    rv = client.get('/usuario/cadastrar', follow_redirects=True)
    assert b'CPF:' in rv.data
    assert b'Nome:' in rv.data
    assert b'Sobrenome:' in rv.data
    assert b'E-mail:' in rv.data
    assert b'Senha:' in rv.data


def test_12_coletor_listar(client):
    rv = client.get('/usuario/listar', follow_redirects=True)
    assert rv.status_code == 200

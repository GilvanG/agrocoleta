from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired


class ColetorForm(FlaskForm):
    cpf = StringField('CPF: ', validators=[DataRequired()])
    nome = StringField('Nome: ', validators=[DataRequired()])
    sobrenome = StringField(u'Sobrenome: ', validators=[DataRequired()])
    email = EmailField('E-mail: ', validators=[DataRequired()])
    senha = PasswordField('Senha: ', validators=[DataRequired()])
    enviar = SubmitField('CADASTRAR')

from database import db


class Campo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(20), unique=False, nullable=False)
    descricao = db.Column(db.String(200), nullable=True)
    coleta_produto = db.relationship('Coleta', backref='campo', lazy=True)

    def asdict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
